﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LabAspNetMvc.Lojinha.Models;
using LabAspNetMvc.Lojinha.Factory.Factories;

namespace LabAspNetMvc.Lojinha.Factory
{
    public static class Fabrica
    {
        #region ClienteFactory

        /// <summary>
        /// Método para criar uma lista de clientes com 100 registros
        /// </summary>
        /// <returns></returns>
        public static IList<ClienteModel> GetListaClientesTop100()
        {
            return new ClienteFactory().GetListaClientesTop100();
        }

        /// <summary>
        /// Método para criar um cliente
        /// </summary>
        /// <returns></returns>
        public static ClienteModel GetCliente()
        {
            return new ClienteFactory().GetCliente();
        }

        #endregion

        #region EnderecoClienteFactory

        /// <summary>
        /// Método para criar uma lista de endereços
        /// </summary>
        /// <returns></returns>
        public static IList<EnderecoClienteModel> GetListaEnderecoClienteTop5()
        {
            return new EnderecoClienteFactory().GetListaEnderecoClienteTop5();
        }

        /// <summary>
        /// Método para criar um endereço e retorna-lo
        /// </summary>
        /// <returns></returns>
        public static EnderecoClienteModel GetEnderecoCliente()
        {
            return new EnderecoClienteFactory().GetEnderecoCliente();
        }
        #endregion
    }
}