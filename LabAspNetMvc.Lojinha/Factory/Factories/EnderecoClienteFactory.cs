﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LabAspNetMvc.Lojinha.Models;

namespace LabAspNetMvc.Lojinha.Factory.Factories
{
    public class EnderecoClienteFactory
    {
        /// <summary>
        /// Método para criar uma lista de endereços
        /// </summary>
        /// <returns></returns>
        public IList<EnderecoClienteModel> GetListaEnderecoClienteTop5()
        {
            var lista = new List<EnderecoClienteModel>();
            for(var i = 0; i < 5; i++)
            {
                var endereco = new EnderecoClienteModel()
                {
                    EnderecoClienteId = i,
                    ClienteId = i,
                    Cidade = "São Paulo",
                    Cep = "00000-00",
                    Estado = "SP",
                    Numero = i.ToString(),
                    Titulo = string.Concat("Endereço Teste", i.ToString())
                };
                lista.Add(endereco);
            }
            return lista;
        }

        /// <summary>
        /// Método para criar um endereço e retorna-lo
        /// </summary>
        /// <returns></returns>
        public EnderecoClienteModel GetEnderecoCliente()
        {
            var endereco = new EnderecoClienteModel()
            {
                EnderecoClienteId = 1,
                ClienteId = 1,
                Cidade = "São Paulo",
                Cep = "00000-00",
                Estado = "SP",
                Numero = "1",
                Titulo = "Endereço Teste 1",
                Logradouro = "Logradouro Teste"

            };
            return endereco;
        }
    }
}