﻿using System;
using System.Collections.Generic;
using LabAspNetMvc.Lojinha.Models;

namespace LabAspNetMvc.Lojinha.Factory.Factories
{
    public class ClienteFactory
    {
        /// <summary>
        /// Método para criar uma lista de clientes com 100 registros
        /// </summary>
        /// <returns></returns>
        public IList<ClienteModel> GetListaClientesTop100()
        {
            var lista = new List<ClienteModel>();
            for (var i = 0; i < 100; i++)
            {
                var cliente = new ClienteModel()
                {
                    ClienteId = i,
                    Cpf = "460.634.257-99", //CPF Random
                    DataNascimento = RandomDay(),
                    Nome = string.Concat("Cliente ", i),
                };

                var endereco = new EnderecoClienteModel()
                {
                    EnderecoClienteId = i,
                    ClienteId = cliente.ClienteId,
                    Cidade = "São Paulo",
                    Cep = "00000-00",
                    Estado = "SP",
                    Numero = i.ToString(),
                    Titulo = string.Concat("Endereço do ", cliente.Nome)
                };
                cliente.Enderecos = new List<EnderecoClienteModel>() { endereco };

                lista.Add(cliente);
            }

            return lista;
        }

        /// <summary>
        /// Método para criar um cliente
        /// </summary>
        /// <returns></returns>
        public ClienteModel GetCliente()
        {
            var cliente = new ClienteModel()
            {
                ClienteId = 1,
                Cpf = "460.634.257-99", //CPF Random
                DataNascimento = RandomDay(),
                Nome = "Cliente Teste",
            };

            var endereco = new EnderecoClienteModel()
            {
                EnderecoClienteId = 1,
                ClienteId = cliente.ClienteId,
                Cidade = "São Paulo",
                Cep = "00000-00",
                Estado = "SP",
                Numero = "000",
                Titulo = string.Concat("Endereço do ", cliente.Nome)
            };
            cliente.Enderecos = new List<EnderecoClienteModel>() { endereco };

            return cliente;
        }

        /// <summary>
        /// Método para gerar datas randômicas
        /// </summary>
        private Random gen = new Random();
        private DateTime RandomDay()
        {
            DateTime start = new DateTime(1995, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(gen.Next(range));
        }
    }
}

