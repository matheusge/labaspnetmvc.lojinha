﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LabAspNetMvc.Lojinha.Startup))]
namespace LabAspNetMvc.Lojinha
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
