﻿using System.Collections.Generic;
using System.Web.Mvc;
using LabAspNetMvc.Lojinha.Models;
using LabAspNetMvc.Lojinha.Factory;

namespace LabAspNetMvc.Lojinha.Controllers
{
    public class ClienteController : Controller
    {
        #region Cliente

        public ActionResult Index()
        {
            var clientes = Fabrica.GetListaClientesTop100();
            return View(clientes);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ClienteModel model)
        {
            if (ModelState.IsValid)
                return RedirectToAction(string.Concat("/Edit/", model.ClienteId));
            return View(model);
        }

        public ActionResult Edit(int? id)
        {
            var cliente = Fabrica.GetCliente();
            return View(cliente);
        }

        [HttpPost]
        public ActionResult Edit(ClienteModel model)
        {
            if (ModelState.IsValid)
                return RedirectToAction("/Index");

            //Chapéu
            model.Enderecos = new List<EnderecoClienteModel>();
            return View(model);
        }

        #endregion

        #region EnderecoCliente

        public ActionResult CreateEndereco(int? clienteId)
        {
            return View(new EnderecoClienteModel());
        }

        [HttpPost]
        public ActionResult CreateEndereco(EnderecoClienteModel model)
        {
            if (ModelState.IsValid)
                return RedirectToAction(string.Concat("/Edit/", model.ClienteId));
            return View(model);
        }

        public ActionResult EditEndereco(int? id)
        {
            var endereco = Fabrica.GetEnderecoCliente();
            return View(endereco);
        }

        [HttpPost]
        public ActionResult EditEndereco(EnderecoClienteModel model)
        {
            if (ModelState.IsValid)
                return RedirectToAction(string.Concat("/Edit/", model.ClienteId));
            return View(model);
        }

        public ActionResult DetailsEndereco(int? id)
        {
            var endereco = Fabrica.GetEnderecoCliente();
            return View(endereco);
        }

        public ActionResult DeleteEndereco(int? id)
        {
            var endereco = Fabrica.GetEnderecoCliente();
            return View(endereco);
        }

        [HttpPost]
        public ActionResult DeleteEndereco(EnderecoClienteModel model)
        {
            return RedirectToAction("Edit/", model.ClienteId);
        }

        #endregion
    }
}