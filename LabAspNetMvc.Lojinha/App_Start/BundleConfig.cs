﻿using System.Web;
using System.Web.Optimization;

namespace LabAspNetMvc.Lojinha
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            //Template Gentellela Alela
            bundles.Add(new StyleBundle("~/Template/css").Include(
                      "~/Public/vendors/bootstrap/dist/css/bootstrap.min.css",
                      "~/Public/vendors/font-awesome/css/font-awesome.min.css",
                      "~/Public/vendors/nprogress/nprogress.css",
                      "~/Public/vendors/iCheck/skins/flat/green.css",
                      "~/Public/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css",
                      "~/Public/vendors/jqvmap/dist/jqvmap.min.css",
                      "~/Public/vendors/bootstrap-daterangepicker/daterangepicker.css",
                      "~/Public/build/css/custom.min.css"));

            bundles.Add(new ScriptBundle("~/Template/js").Include(
                        "~/Public/vendors/jquery/dist/jquery.min.js",
                        "~/Public/vendors/bootstrap/dist/js/bootstrap.min.js",
                        "~/Public/vendors/fastclick/lib/fastclick.js",
                        "~/Public/vendors/nprogress/nprogress.js",
                        "~/Public/vendors/Chart.js/dist/Chart.min.js",
                        "~/Public/vendors/gauge.js/dist/gauge.min.js",
                        "~/Public/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js",
                        "~/Public/vendors/iCheck/icheck.min.js",
                        "~/Public/vendors/skycons/skycons.js",
                        "~/Public/vendors/Flot/jquery.flot.js",
                        "~/Public/vendors/Flot/jquery.flot.pie.js",
                        "~/Public/vendors/Flot/jquery.flot.time.js",
                        "~/Public/vendors/Flot/jquery.flot.stack.js",
                        "~/Public/vendors/Flot/jquery.flot.resize.js",
                        "~/Public/vendors/flot.orderbars/js/jquery.flot.orderBars.js",
                        "~/Public/vendors/flot-spline/js/jquery.flot.spline.min.js",
                        "~/Public/vendors/flot.curvedlines/curvedLines.js",
                        "~/Public/vendors/DateJS/build/date.js",
                        "~/Public/vendors/jqvmap/dist/jquery.vmap.js",
                        "~/Public/vendors/jqvmap/dist/maps/jquery.vmap.world.js",
                        "~/ Public/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js",
                        "~/Public/vendors/moment/min/moment.min.js",
                        "~/Public/vendors/bootstrap-daterangepicker/daterangepicker.js",
                        "~/Public/build/js/custom.min.js"
                       ));
        }
    }
}
