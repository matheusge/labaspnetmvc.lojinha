﻿using System.Web;
using System.Web.Mvc;

namespace LabAspNetMvc.Lojinha
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
