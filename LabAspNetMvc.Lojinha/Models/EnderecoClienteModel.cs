﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace LabAspNetMvc.Lojinha.Models
{
    public class EnderecoClienteModel
    {
        [DisplayName("Endereço Id")]
        [Key]
        public int EnderecoClienteId { get; set; }

        [DisplayName("Título")]
        [Required]
        public String Titulo { get; set; }

        [DisplayName("CEP")]
        [Required]
        public String Cep { get; set; }

        [Required]
        public String Logradouro { get; set; }

        public String Complemento { get; set; }

        [DisplayName("Número")]
        [Required]
        public String Numero { get; set; }

        [Required]
        public String Cidade { get; set; }

        [Required]
        public String Estado { get; set; }

        public int ClienteId { get; set; }
    }
}