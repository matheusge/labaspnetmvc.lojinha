﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace LabAspNetMvc.Lojinha.Models
{
    public class ClienteModel
    {
        [DisplayName("Cliente Id")]
        [Key]
        public int ClienteId { get; set; }

        [Required]
        public String Nome { get; set; }

        [DisplayName("Data de Nascimento")]
        [Required]
        public DateTime DataNascimento { get; set; }

        [DisplayName("CPF")]
        [Required]
        public String Cpf { get; set; }

        public IList<EnderecoClienteModel> Enderecos { get; set; }
    }
}